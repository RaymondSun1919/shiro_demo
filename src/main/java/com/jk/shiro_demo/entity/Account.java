package com.jk.shiro_demo.entity;

import lombok.Data;
import lombok.Getter;

import java.io.Serializable;

/**
 * (Account)实体类
 *
 * @author raymond
 * @since 2023-09-08 09:35:28
 */
@Data
public class Account implements Serializable {
    private static final long serialVersionUID = 318984361066355902L;

    private Integer id;

    private String username;

    private String password;

}

