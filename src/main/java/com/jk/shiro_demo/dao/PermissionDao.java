package com.jk.shiro_demo.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 权限(Permission)表数据库访问层
 *
 * @author raymond
 * @since 2023-09-08 09:58:06
 */
@Mapper
public interface PermissionDao {

    List<String> selectPermissionByLoginId(Integer loginId);

}

