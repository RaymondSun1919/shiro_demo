package com.jk.shiro_demo.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 角色(Role)表数据库访问层
 *
 * @author raymond
 * @since 2023-09-08 09:58:46
 */
@Mapper
public interface RoleDao {
    List<String> selectRolesByLoginId(Integer loginId);

}

