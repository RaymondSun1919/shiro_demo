package com.jk.shiro_demo.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import com.jk.shiro_demo.entity.Account;
import com.jk.shiro_demo.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Account)表控制层
 *
 * @author raymond
 * @since 2023-09-07 13:58:58
 */
@RestController
@RequestMapping("account")
public class AccountController {
    /**
     * 服务对象
     */
    @Resource
    private AccountService accountService;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @PostMapping("/login")
    public SaResult login(@RequestBody Account account){
        log.info(account.toString());
        account=this.accountService.login(account.getUsername(), account.getPassword());
        log.info(account.toString());
        if (account!=null){
            StpUtil.login(account.getId());
            SaTokenInfo saTokenInfo = StpUtil.getTokenInfo();
            log.info(saTokenInfo.getLoginId().getClass().getName());
            return SaResult.data(saTokenInfo);
        }else{
            return SaResult.error();
        }
    }

    @SaCheckLogin
    @GetMapping("/logout")
    public SaResult logout(){
        Integer id=StpUtil.getLoginIdAsInt();
        StpUtil.logout(id);
        return SaResult.ok();
    }

    @SaCheckLogin
    @GetMapping("/getRoles")
    public SaResult getRoles (){
        Integer id=StpUtil.getLoginIdAsInt();
        List<String> roleList = StpUtil.getRoleList(id);
        return SaResult.ok(roleList.toString());
    }

    @SaCheckPermission("select")
    @GetMapping("selectUser")
    public List<Account> selectUser(){
        Integer id=StpUtil.getLoginIdAsInt();
        return this.accountService.selectUserByManagerId(id);
    }

    @SaCheckPermission("add")
    @PostMapping
    public SaResult add(@RequestBody Account account){
        Integer id=StpUtil.getLoginIdAsInt();
        return SaResult.ok(this.accountService.add(account,id).toString());
    }
}

