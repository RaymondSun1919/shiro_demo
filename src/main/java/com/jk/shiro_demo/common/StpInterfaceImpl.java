package com.jk.shiro_demo.common;

import cn.dev33.satoken.stp.StpInterface;
import com.jk.shiro_demo.service.PermissionService;
import com.jk.shiro_demo.service.RoleService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class StpInterfaceImpl implements StpInterface {

    @Resource
    RoleService roleService;

    @Resource
    PermissionService permissionService;

    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        Integer id = Integer.parseInt((String)loginId);
        return permissionService.selectPermissionByLoginId(id);
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        return this.roleService.selectRolesByLoginId((Integer)loginId);
    }
}
