package com.jk.shiro_demo.service;

import java.util.List;

/**
 * 角色(Role)表服务接口
 *
 * @author raymond
 * @since 2023-09-08 09:58:46
 */
public interface RoleService {

    List<String> selectRolesByLoginId(Integer loginId);
}
