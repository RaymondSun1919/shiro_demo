package com.jk.shiro_demo.service.impl;
import com.jk.shiro_demo.entity.Account;
import com.jk.shiro_demo.dao.AccountDao;
import com.jk.shiro_demo.service.AccountService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * (Account)表服务实现类
 *
 * @author raymond
 * @since 2023-09-07 13:58:58
 */
@Service("accountService")
public class AccountServiceImpl implements AccountService {
    @Resource
    private AccountDao accountDao;

    @Override
    public Account login(String username, String password) {
        return this.accountDao.queryByUserName(username,password);
    }

    @Override
    public List<Account> selectUserByManagerId(Integer id) {
        return this.accountDao.selectUserByManagerId(id);
    }

    @Override
    public Integer add(Account account, Integer id) {
        return accountDao.add(account,id);
    }
}
