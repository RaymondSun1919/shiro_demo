package com.jk.shiro_demo.service.impl;
import com.jk.shiro_demo.dao.PermissionDao;
import com.jk.shiro_demo.service.PermissionService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * 权限(Permission)表服务实现类
 *
 * @author raymond
 * @since 2023-09-08 09:58:06
 */
@Service("permissionService")
public class PermissionServiceImpl implements PermissionService {
    @Resource
    private PermissionDao permissionDao;

    @Override
    public List<String> selectPermissionByLoginId(Integer loginId) {
        return this.permissionDao.selectPermissionByLoginId(loginId);
    }
}
