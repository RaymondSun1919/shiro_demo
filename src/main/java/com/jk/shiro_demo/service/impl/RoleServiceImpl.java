package com.jk.shiro_demo.service.impl;
import com.jk.shiro_demo.dao.RoleDao;
import com.jk.shiro_demo.service.RoleService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * 角色(Role)表服务实现类
 *
 * @author raymond
 * @since 2023-09-08 09:58:46
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleDao roleDao;

    @Override
    public List<String> selectRolesByLoginId(Integer loginId) {
        return this.roleDao.selectRolesByLoginId(loginId);
    }
}
