package com.jk.shiro_demo.service;

import java.util.List;

/**
 * 权限(Permission)表服务接口
 *
 * @author raymond
 * @since 2023-09-08 09:58:06
 */
public interface PermissionService {
    List<String> selectPermissionByLoginId(Integer loginId);
}
