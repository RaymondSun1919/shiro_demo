package com.jk.shiro_demo.service;

import com.jk.shiro_demo.entity.Account;

import java.util.List;

/**
 * (Account)表服务接口
 *
 * @author raymond
 * @since 2023-09-07 13:58:58
 */
public interface AccountService {

    Account login(String username, String password);

    List<Account> selectUserByManagerId(Integer id);

    Integer add(Account account, Integer id);
}
